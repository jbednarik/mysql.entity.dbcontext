﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySql.Entity
{
    public class MySqlDbContext : DbContext
    {
        public MySqlDbContext()
        {
            InitializeDataProvidersInAppConfig();
        }

        public MySqlDbContext(DbCompiledModel model)
            : base(model)
        {
            InitializeDataProvidersInAppConfig();
        }

        public MySqlDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            InitializeDataProvidersInAppConfig();
        }

        public MySqlDbContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
            InitializeDataProvidersInAppConfig();
        }

        public MySqlDbContext(ObjectContext objecContext, bool dbContextOwnsObjectContext)
            : base(objecContext, dbContextOwnsObjectContext)
        {
            InitializeDataProvidersInAppConfig();
        }

        public MySqlDbContext(string nameOrConnectionString, DbCompiledModel model)
            : base(nameOrConnectionString, model)
        {
            InitializeDataProvidersInAppConfig();
        }

        public MySqlDbContext(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection)
            : base(existingConnection, model, contextOwnsConnection)
        {
            InitializeDataProvidersInAppConfig();
        }

        protected virtual void InitializeDataProvidersInAppConfig()
        {
            const string dataProvider = @"MySql.Data.MySqlClient";
            const string dataProviderDescription = @".Net Framework Data Provider for MySQL";
            const string dataProviderName = @"MySQL Data Provider";
            const string dataProviderType = @"MySql.Data.MySqlClient.MySqlClientFactory, MySql.Data";

            const string systemDataSection = @"system.data";
            const string invariantNameString = @"InvariantName";


            bool addProvider = true;
            var dataSet = ConfigurationManager.GetSection(systemDataSection) as DataSet;
            foreach (DataRow row in dataSet.Tables[0].Rows)
            {
                if ((row[invariantNameString] as string) == dataProvider)
                {
                    // it is already in the config, no need to add.
                    addProvider = false;
                    break;
                }
            }

            if (addProvider)
                dataSet.Tables[0].Rows.Add(dataProviderName, dataProviderDescription, dataProvider, dataProviderType);

        }
    }
}
